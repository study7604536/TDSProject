// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/StateEffects/TDS_StateEffect.h"

#include <Particles/ParticleSystemComponent.h>

#include "GameFramework/Character.h"
#include "TDS/Game/TDS_IGameActor.h"
#include "TDS/Character/TDSHealthComponent.h"
#include "TDS/Character/TDSCharacterHealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TDS/Weapons/WeaponDefault.h"
#include "Widgets/Text/ISlateEditableTextWidget.h"
#include <TDS/Character/TDSCharacter.h>

bool UTDS_StateEffect::InitObject(AActor* Actor)
{
	
	myActor = Actor;

	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffects(this);
	}

	return true;
}


void UTDS_StateEffect::DestroyObject()
{
	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffects(this);
	}
	
	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

bool UTDS_StateEffect_ExecuteOnce::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	ExecuteOnce();
	return true;

}

void UTDS_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UTDSHealthComponent* myHealthComp = Cast<UTDSHealthComponent>(
			myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}


	DestroyObject();
}


bool UTDS_StateEffect_ExecuteTimer::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDS_StateEffect_ExecuteTimer::DestroyObject, Timer,
	                                       false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTDS_StateEffect_ExecuteTimer::Execute, RateTime,
	                                       true);
	if (immortal)
    {
        UTDSCharacterHealthComponent* ShieldComp = Cast<UTDSCharacterHealthComponent>(
            Actor->GetComponentByClass(UTDSCharacterHealthComponent::StaticClass()));
        if (ShieldComp)
        {
            OriginalCoefDamage = ShieldComp->CoefDamage; // 
            OriginalShieldCoefDamage = ShieldComp->ShieldCoef;
            ShieldComp->CoefDamage = 0.0f; 
            ShieldComp->ShieldCoef = 0.0f; 
        }
    }

	 ATDSCharacter* Character = Cast<ATDSCharacter>(myActor->GetComponentByClass(ATDSCharacter::StaticClass()));
	if (Character)
    {
		OriginalSpeedMultiplier = Character->SpeedMultiplier;
        Character->SpeedMultiplier = SpeedMultiplierEffect; 
        Character->CharacterUpdate(); 
    }

	if (ParticleEffect)
	{
		FName NameBoneAttached;
		FVector Loc = FVector(0);

		ParticleEmiter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneAttached, Loc,
		                                       FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
	}

	return true;
}

void UTDS_StateEffect_ExecuteTimer::DestroyObject()
{
	if (immortal)
	{
        UTDSCharacterHealthComponent* ShieldComp = Cast<UTDSCharacterHealthComponent>(
            myActor->GetComponentByClass(UTDSCharacterHealthComponent::StaticClass()));
        if (ShieldComp)
        {
            ShieldComp->CoefDamage = OriginalCoefDamage;
            ShieldComp->ShieldCoef = OriginalShieldCoefDamage; 
		}
		AWeaponDefault* Weapon = Cast<AWeaponDefault>(myActor->GetComponentByClass(AWeaponDefault::StaticClass()));
		if (Weapon)
		{
			Weapon->AdditionalWeaponInfo.Round = RoundOriginal;
		}

	}
	
	 ATDSCharacter* Character = Cast<ATDSCharacter>(myActor->GetComponentByClass(ATDSCharacter::StaticClass()));
	if (Character)
    {
		Character->SpeedMultiplier = OriginalSpeedMultiplier;
        Character->CharacterUpdate(); 
    }


	if (ParticleEmiter)
	{
		ParticleEmiter->DestroyComponent();
		ParticleEmiter = nullptr;
	}
	Super::DestroyObject();
	

}
void UTDS_StateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		UTDSHealthComponent* myHealthComp = Cast<UTDSHealthComponent>(
			myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
		if (immortal)
		{
			AWeaponDefault* Weapon = Cast<AWeaponDefault>(myActor->GetComponentByClass(AWeaponDefault::StaticClass()));
			if (Weapon)
			{
				RoundOriginal = Weapon->AdditionalWeaponInfo.Round;
				Weapon->AdditionalWeaponInfo.Round = 99;
			}
		}
		
	}
}
