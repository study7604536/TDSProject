// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Game/TDS_IGameActor.h"

// Add default functionality here for any ITDS_IGameActor functions that are not pure virtual.

EPhysicalSurface ITDS_IGameActor::GetSurfaceType()
{
	return EPhysicalSurface::SurfaceType_Default;
}

TArray<UTDS_StateEffect*> ITDS_IGameActor::GetAllCurrentEffects()
{
	TArray<UTDS_StateEffect*> Effects;
	return Effects;
}

void ITDS_IGameActor::RemoveEffects(UTDS_StateEffect* RemoveEffect)
{
}

void ITDS_IGameActor::AddEffects(UTDS_StateEffect* newEffect)
{
}
 