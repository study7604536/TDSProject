// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Character/TDSInventoryComponent.h"
#include "TDS/Game/TDSGameInstance.h"

// Sets default values for this component's properties
UTDSInventoryComponent::UTDSInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTDSInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	///...
	for (int8 i = 0; i < WeaponSlot.Num(); i++)
	{
		UTDSGameInstance* myGi = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
		if (myGi)
		{
			if (!WeaponSlot[i].NameItem.IsNone())
			{
				FWeaponInfo Info;
				if (myGi->GetWeaponInfoByName(WeaponSlot[i].NameItem, Info))
					WeaponSlot[i].AdditionalInfo.Round = Info.MaxRound;
				else
				{
					//WeaponSlot.RemoveAt(i);
					//i--;
				}
			}
		}
	}

	MaxWeaponSlots = WeaponSlot.Num();

	if (WeaponSlot.IsValidIndex(0))
	{
		if (!WeaponSlot[0].NameItem.IsNone())
			OnSwitchWeapon.Broadcast(WeaponSlot[0].NameItem, WeaponSlot[0].AdditionalInfo, 0);
	}
}


// Called every frame
void UTDSInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType,
                                           FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UTDSInventoryComponent::SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo,
                                                 bool bIsForward)
{
	bool BIsSuccess = false;
	int8 CorrectIndex = ChangeToIndex;
	if (ChangeToIndex > WeaponSlot.Num() - 1)
		CorrectIndex = 0;
	else if (ChangeToIndex < 0)
		CorrectIndex = WeaponSlot.Num() - 1;

	FName NewIdWeapon;
	FAdditionalWeaponInfo NewAdditionalWeaponInfo;
	int32 NewCurrentIndex = 0;

	if (WeaponSlot.IsValidIndex(CorrectIndex))
	{
		if (!WeaponSlot[CorrectIndex].NameItem.IsNone())
		{
			if (WeaponSlot[CorrectIndex].AdditionalInfo.Round > 0)
			{
				BIsSuccess = true;
			}
			else
			{
				UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
				if (myGI)
				{
					//Check Ammo slots for weapon
					FWeaponInfo myInfo;
					myGI->GetWeaponInfoByName(WeaponSlot[CorrectIndex].NameItem, myInfo);

					bool bIsFind = false;
					int j = 0;
					while (j < AmmoSlot.Num() && !bIsFind)
					{
						if (AmmoSlot[j].WeaponType == myInfo.WeaponType && AmmoSlot[j].Count > 0)
						{
							BIsSuccess = true;
							bIsFind = true;
						}
						j++;
					}
				}
			}
			if (BIsSuccess)
			{
				NewCurrentIndex = CorrectIndex;
				NewIdWeapon = WeaponSlot[CorrectIndex].NameItem;
				NewAdditionalWeaponInfo = WeaponSlot[CorrectIndex].AdditionalInfo;
			}
		}
	}
	if (!BIsSuccess)
	{
		if (bIsForward)
		{
			int8 iteration = 0;
			int8 Seconditeration = 0;
			while (iteration < WeaponSlot.Num() && !BIsSuccess)
			{
				iteration++;
				int8 tmpIndex = ChangeToIndex + iteration;
				if (WeaponSlot.IsValidIndex(tmpIndex))
				{
					if (!WeaponSlot[tmpIndex].NameItem.IsNone())
					{
						if (WeaponSlot[tmpIndex].AdditionalInfo.Round > 0)
						{
							//WeaponGood
							BIsSuccess = true;
							NewIdWeapon = WeaponSlot[tmpIndex].NameItem;
							NewAdditionalWeaponInfo = WeaponSlot[tmpIndex].AdditionalInfo;
							NewCurrentIndex = tmpIndex;
						}
						else
						{
							FWeaponInfo myInfo;
							UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());

							myGI->GetWeaponInfoByName(WeaponSlot[tmpIndex].NameItem, myInfo);

							bool bIsFind = false;
							int8 j = 0;
							while (j < AmmoSlot.Num() && !bIsFind)
							{
								if (AmmoSlot[j].WeaponType == myInfo.WeaponType && AmmoSlot[j].Count > 0)
								{
									//WeaponGood
									BIsSuccess = true;
									NewIdWeapon = WeaponSlot[tmpIndex].NameItem;
									NewAdditionalWeaponInfo = WeaponSlot[tmpIndex].AdditionalInfo;
									NewCurrentIndex = tmpIndex;
									bIsFind = true;
								}
								j++;
							}
						}
					}
				}
				else
				{
					//go to end of right of array weapon slots
					if (OldIndex != Seconditeration)
					{
						if (WeaponSlot.IsValidIndex(Seconditeration))
						{
							if (!WeaponSlot[Seconditeration].NameItem.IsNone())
							{
								if (WeaponSlot[Seconditeration].AdditionalInfo.Round > 0)
								{
									//WeaponGood
									BIsSuccess = true;
									NewIdWeapon = WeaponSlot[Seconditeration].NameItem;
									NewAdditionalWeaponInfo = WeaponSlot[Seconditeration].AdditionalInfo;
									NewCurrentIndex = Seconditeration;
								}
								else
								{
									FWeaponInfo myInfo;
									UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());

									myGI->GetWeaponInfoByName(WeaponSlot[Seconditeration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;
									while (j < AmmoSlot.Num() && !bIsFind)
									{
										if (AmmoSlot[j].WeaponType == myInfo.WeaponType && AmmoSlot[j].Count > 0)
										{
											//WeaponGood
											BIsSuccess = true;
											NewIdWeapon = WeaponSlot[Seconditeration].NameItem;
											NewAdditionalWeaponInfo = WeaponSlot[Seconditeration].AdditionalInfo;
											NewCurrentIndex = Seconditeration;
											bIsFind = true;
										}
										j++;
									}
								}
							}
						}
					}
					else
					{
						//go to same weapon when start
						if (WeaponSlot.IsValidIndex(Seconditeration))
						{
							if (!WeaponSlot[Seconditeration].NameItem.IsNone())
							{
								if (WeaponSlot[Seconditeration].AdditionalInfo.Round > 0)
								{
									//WeaponGood, it same weapon do nothing
								}
								else
								{
									FWeaponInfo myInfo;
									UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());

									myGI->GetWeaponInfoByName(WeaponSlot[Seconditeration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;
									while (j < AmmoSlot.Num() && !bIsFind)
									{
										if (AmmoSlot[j].WeaponType == myInfo.WeaponType)
										{
											if (AmmoSlot[j].Count > 0)
											{
												//WeaponGood, it same weapon do nothing
											}
											else
											{
												//Not find weapon with amm need init Pistol with infinity ammo
												UE_LOG(LogTemp, Error,
												       TEXT(
													       "UTPSInventoryComponent::SwitchWeaponToIndex - Init PISTOL - NEED"
												       ));
											}
										}
										j++;
									}
								}
							}
						}
					}
					Seconditeration++;
				}
			}
		}
		else
		{
			int8 iteration = 0;
			int8 Seconditeration = WeaponSlot.Num() - 1;
			while (iteration < WeaponSlot.Num() && !BIsSuccess)
			{
				iteration++;
				int8 tmpIndex = ChangeToIndex - iteration;
				if (WeaponSlot.IsValidIndex(tmpIndex))
				{
					if (!WeaponSlot[tmpIndex].NameItem.IsNone())
					{
						if (WeaponSlot[tmpIndex].AdditionalInfo.Round > 0)
						{
							//WeaponGood
							BIsSuccess = true;
							NewIdWeapon = WeaponSlot[tmpIndex].NameItem;
							NewAdditionalWeaponInfo = WeaponSlot[tmpIndex].AdditionalInfo;
							NewCurrentIndex = tmpIndex;
						}
						else
						{
							FWeaponInfo myInfo;
							UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());

							myGI->GetWeaponInfoByName(WeaponSlot[tmpIndex].NameItem, myInfo);

							bool bIsFind = false;
							int8 j = 0;
							while (j < AmmoSlot.Num() && !bIsFind)
							{
								if (AmmoSlot[j].WeaponType == myInfo.WeaponType && AmmoSlot[j].Count > 0)
								{
									//WeaponGood
									BIsSuccess = true;
									NewIdWeapon = WeaponSlot[tmpIndex].NameItem;
									NewAdditionalWeaponInfo = WeaponSlot[tmpIndex].AdditionalInfo;
									NewCurrentIndex = tmpIndex;
									bIsFind = true;
								}
								j++;
							}
						}
					}
				}
				else
				{
					//go to end of LEFT of array weapon slots
					if (OldIndex != Seconditeration)
					{
						if (WeaponSlot.IsValidIndex(Seconditeration))
						{
							if (!WeaponSlot[Seconditeration].NameItem.IsNone())
							{
								if (WeaponSlot[Seconditeration].AdditionalInfo.Round > 0)
								{
									//WeaponGood
									BIsSuccess = true;
									NewIdWeapon = WeaponSlot[Seconditeration].NameItem;
									NewAdditionalWeaponInfo = WeaponSlot[Seconditeration].AdditionalInfo;
									NewCurrentIndex = Seconditeration;
								}
								else
								{
									FWeaponInfo myInfo;
									UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());

									myGI->GetWeaponInfoByName(WeaponSlot[Seconditeration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;
									while (j < AmmoSlot.Num() && !bIsFind)
									{
										if (AmmoSlot[j].WeaponType == myInfo.WeaponType && AmmoSlot[j].Count > 0)
										{
											//WeaponGood
											BIsSuccess = true;
											NewIdWeapon = WeaponSlot[Seconditeration].NameItem;
											NewAdditionalWeaponInfo = WeaponSlot[Seconditeration].AdditionalInfo;
											NewCurrentIndex = Seconditeration;
											bIsFind = true;
										}
										j++;
									}
								}
							}
						}
					}
					else
					{
						//go to same weapon when start
						if (WeaponSlot.IsValidIndex(Seconditeration))
						{
							if (!WeaponSlot[Seconditeration].NameItem.IsNone())
							{
								if (WeaponSlot[Seconditeration].AdditionalInfo.Round > 0)
								{
									//WeaponGood, it same weapon do nothing
								}
								else
								{
									FWeaponInfo myInfo;
									UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());

									myGI->GetWeaponInfoByName(WeaponSlot[Seconditeration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;
									while (j < AmmoSlot.Num() && !bIsFind)
									{
										if (AmmoSlot[j].WeaponType == myInfo.WeaponType)
										{
											if (AmmoSlot[j].Count > 0)
											{
												//WeaponGood, it same weapon do nothing
											}
											else
											{
												//Not find weapon with amm need init Pistol with infinity ammo
												UE_LOG(LogTemp, Error,
												       TEXT(
													       "UTPSInventoryComponent::SwitchWeaponToIndex - Init PISTOL - NEED"
												       ));
											}
										}
										j++;
									}
								}
							}
						}
					}
					Seconditeration--;
				}
			}
		}
	}
	if (BIsSuccess)
	{
		SetAdditionalWeaponInfo(OldIndex, OldInfo);
		OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalWeaponInfo, NewCurrentIndex);
	}
	return BIsSuccess;
}

FAdditionalWeaponInfo UTDSInventoryComponent::GetAdditionalInfoWeapon(int32 IndexWeapon)
{
	FAdditionalWeaponInfo result;
	if (WeaponSlot.IsValidIndex(IndexWeapon))
	{
		bool BIsFind = false;
		int8 i = 0;
		while (i < WeaponSlot.Num() && !BIsFind)
		{
			if (i == IndexWeapon)
			{
				result = WeaponSlot[i].AdditionalInfo;
				BIsFind = true;
			}
			i++;
		}
		if (!BIsFind)
		{
			UE_LOG(LogTemp, Warning,
			       TEXT("UTDSInventoryComponent::GetAdditionalInfoWeapon - Not Found weapon with index -%d" ),
			       IndexWeapon);
		}
		else
			UE_LOG(LogTemp, Warning,
		       TEXT("UTDSInventoryComponent::GetAdditionalInfoWeapon - Not Correct weapon with index -%d" ),
		       IndexWeapon);
	}
	return result;
}

int32 UTDSInventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponsName)
{
	int32 result = -1;
	int8 i = 0;
	bool bIsFind = false;
	while (i < WeaponSlot.Num() && !bIsFind)
	{
		if (WeaponSlot[i].NameItem == IdWeaponsName)
		{
			bIsFind = true;
			result = i;
		}
		i++;
	}
	return result;
}

FName UTDSInventoryComponent::GetWeaponNameBySlotIndex(int32 IndexSlot)
{
	FName result;

	if (WeaponSlot.IsValidIndex(IndexSlot))
		result = WeaponSlot[IndexSlot].NameItem;

	return result;
}

void UTDSInventoryComponent::SetAdditionalWeaponInfo(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	if (WeaponSlot.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlot.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				WeaponSlot[i].AdditionalInfo = NewInfo;
				bIsFind = true;

				OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo);
			}
			i++;
		}
		if (!bIsFind)
		{
			UE_LOG(LogTemp, Warning,
			       TEXT("UTDSInventoryComponent::SetAdditionalWeaponInfo - No weapon found with index -%d" ),
			       IndexWeapon);
		}
	}
	else UE_LOG(LogTemp, Warning,
	            TEXT("UTDSInventoryComponent::SetAdditionalWeaponInfo - Not correct weapon index -%d" ), IndexWeapon);
}

void UTDSInventoryComponent::AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CountChangeAmmo)
{
	bool bIsfind = false;
	int8 i = 0;
	while (i < AmmoSlot.Num() && !bIsfind)
	{
		if (AmmoSlot[i].WeaponType == TypeWeapon)
		{
			AmmoSlot[i].Count += CountChangeAmmo;
			if (AmmoSlot[i].Count > AmmoSlot[i].MaxCount)
				AmmoSlot[i].Count = AmmoSlot[i].MaxCount;

			OnAmmoChange.Broadcast(AmmoSlot[i].WeaponType, AmmoSlot[i].Count);

			bIsfind = true;
		}
		i++;
	}
}

bool UTDSInventoryComponent::CheckAmmoForWeapon(EWeaponType TypeWeapon, int8& AvailableAmmoforWeapon)
{
	AvailableAmmoforWeapon = 0;
	bool BIsFind = false;
	int8 i = 0;
	while (i < AmmoSlot.Num() && !BIsFind)
	{
		if (AmmoSlot[i].WeaponType == TypeWeapon)
		{
			BIsFind = true;
			AvailableAmmoforWeapon = AmmoSlot[i].Count;
			if (AmmoSlot[i].Count > 0)
				return true;
		}
		i++;
	}

	OnWeaponAmmoEmpty.Broadcast(TypeWeapon);

	return false;
}

bool UTDSInventoryComponent::CheckCanTakeWeapon(int32& FreeSlot)
{
	bool bIsFreeSlot = false;
	int8 i = 0;
	while (i < WeaponSlot.Num() && !bIsFreeSlot)
	{
		if (WeaponSlot[i].NameItem.IsNone())
		{
			bIsFreeSlot = true;
			FreeSlot = i;
		}
		i++;
	}

	return bIsFreeSlot;
}

bool UTDSInventoryComponent::CheckCanTakeAmmo(EWeaponType AmmoType)
{
	bool result = false;
	int8 i = 0;
	while (i < AmmoSlot.Num() && !result)
	{
		if (AmmoSlot[i].WeaponType == AmmoType && AmmoSlot[i].Count < AmmoSlot[i].MaxCount)
			result = true;
		i++;
	}
	return result;
}


bool UTDSInventoryComponent::SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot,
                                                     int32 CurrentIndexWeaponChar, FDropItem& DropItemInfo)
{
	bool result = false;
	
	if (WeaponSlot.IsValidIndex(IndexSlot) && GetDropItemInfoFromInventory(IndexSlot, DropItemInfo))
	{
		WeaponSlot[IndexSlot] = NewWeapon;
		SwitchWeaponToIndex(CurrentIndexWeaponChar, -1, NewWeapon.AdditionalInfo, true);
		OnUpdateWeaponSlots.Broadcast(IndexSlot, NewWeapon);
		result = true;
	}
	return result;
}

bool UTDSInventoryComponent::TryGetWeaponToInventory(FWeaponSlot NewWeapon)
{
	int32 IndexSlot = -1;
	if (CheckCanTakeWeapon(IndexSlot))
	{
		WeaponSlot[IndexSlot] = NewWeapon;
		OnUpdateWeaponSlots.Broadcast(IndexSlot, NewWeapon);
		return true;
	}

	return false;
}

bool UTDSInventoryComponent::GetDropItemInfoFromInventory(int32 IndexSlot, FDropItem& DropItemInfo)
{
	bool result = false;
	
	FName DropItemName = GetWeaponNameBySlotIndex(IndexSlot);

	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		result = myGI->GetDropItemInfoByWeaponName(DropItemName, DropItemInfo);
		if (WeaponSlot.IsValidIndex(IndexSlot))
		{
			DropItemInfo.WeaponInfo.AdditionalInfo = WeaponSlot[IndexSlot].AdditionalInfo;
		}
		
	}


	return result;
}
