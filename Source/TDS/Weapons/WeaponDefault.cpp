// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Weapons/WeaponDefault.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "TDS/Character/TDSInventoryComponent.h"
#include "TDS/StateEffects/TDS_StateEffect.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Initialize the scene component and set it as the root component
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	// Initialize the skeletal mesh component and attach it to the root component
	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	// Initialize the static mesh component and attach it to the root component
	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	// Initialize the shoot location component and attach it to the root component
	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit();
}


// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Handle weapon firing logic
	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
	MagDropTick(DeltaTime);
	ShellDropTick(DeltaTime);
}

// Handles the ticking logic for firing the weapon
void AWeaponDefault::FireTick(float DeltaTime)
{
	if (WeaponFiring && GetWeaponRound() > 0 && !WeaponReloading)
		if (FireTimer < 0.0f)
		{
			if (!WeaponReloading)
				Fire();
		}
		else
			FireTimer -= DeltaTime;
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading)
	{
		if (!WeaponFiring)
		{
			if (ShouldReduceDispersion)
			{
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			}
			else
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
		}

		if (CurrentDispersion < CurrentDispersionMin)
		{
			CurrentDispersion = CurrentDispersionMin;
		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
}

void AWeaponDefault::MagDropTick(float DeltaTime)
{
	if (DropMagFlag)
		if (DropMagTimer < 0.0f)
		{
			DropMagFlag = false;
			InitDropMesh(WeaponSetting.MagDropMesh.MeshDrop, WeaponSetting.MagDropMesh.MeshDropOffset,
			             WeaponSetting.MagDropMesh.MeshDropImpulseVector, WeaponSetting.MagDropMesh.MeshDropLifeTime,
			             WeaponSetting.MagDropMesh.ImpulseDispersion, WeaponSetting.MagDropMesh.ImpulsePower,
			             WeaponSetting.MagDropMesh.CustomMass);
		}
		else
			DropMagTimer -= DeltaTime;
}

void AWeaponDefault::ShellDropTick(float DeltaTime)
{
	if (DropShellFlag)
		if (DropShellTimer < 0.0f)
		{
			DropShellFlag = false;
			InitDropMesh(WeaponSetting.Shell.MeshDrop, WeaponSetting.Shell.MeshDropOffset,
			             WeaponSetting.Shell.MeshDropImpulseVector, WeaponSetting.Shell.MeshDropLifeTime,
			             WeaponSetting.Shell.MeshDropLifeTime, WeaponSetting.Shell.ImpulsePower,
			             WeaponSetting.Shell.CustomMass);
		}
		else
			DropShellTimer -= DeltaTime;
}

// Initializes the weapon settings
void AWeaponDefault::WeaponInit()
{
	// Destroy the skeletal mesh component if no mesh is assigned
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	// Destroy the static mesh component if no mesh is assigned
	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}
	UpdateStateWeapon(EMovementState::Run_State);
}

// Sets the weapon's firing state
void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire())
		WeaponFiring = bIsFire;
	else
		WeaponFiring = false;
	FireTimer = 0.01f;
}

// Checks if the weapon can fire
bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}

// Retrieves the projectile information from the weapon settings
FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSetting.ProjectileSetting;
}

// Handles the weapon firing logic
void AWeaponDefault::Fire()
{
	UAnimMontage* Anim = nullptr;
	if (WeaponAiming)
		Anim = WeaponSetting.FAnimInfo.AnimCharacterFireAim;
	else
		Anim = WeaponSetting.FAnimInfo.AnimCharacterFire;

	if (WeaponSetting.FAnimInfo.AnimCharacterFire && SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(WeaponSetting.FAnimInfo.AnimWeaponFire);
	}

	if (WeaponSetting.Shell.MeshDrop)
	{
		if (WeaponSetting.Shell.MeshDropTime < 0.0f)
		{
			InitDropMesh(WeaponSetting.Shell.MeshDrop, WeaponSetting.Shell.MeshDropOffset,
			             WeaponSetting.Shell.MeshDropImpulseVector, WeaponSetting.Shell.MeshDropLifeTime,
			             WeaponSetting.Shell.ImpulseDispersion, WeaponSetting.Shell.ImpulsePower,
			             WeaponSetting.Shell.CustomMass);
		}
		else
		{
			DropShellFlag = true;
			DropShellTimer = WeaponSetting.Shell.MeshDropTime;
		}
	}

	FireTimer = WeaponSetting.RateOfFire;
	AdditionalWeaponInfo.Round = AdditionalWeaponInfo.Round - 1;
	ChangeDispersionByShoot();

	OnWeaponFireStart.Broadcast(Anim);


	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon,
	                                       ShootLocation->GetComponentLocation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon,
	                                         ShootLocation->GetComponentTransform());

	int8 NumberProjectile = GetNumberProjectilesByShot();

	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo = GetProjectile();


		FVector EndLocation;
		for (int8 i = 0; i < NumberProjectile; i++)
		{
			EndLocation = GetFireEndLocation();

			// Spawn and initialize the projectile
			if (ProjectileInfo.Projectile)
			{
				//Projectile Init ballistic fire
				FVector Dir = EndLocation - SpawnLocation;

				Dir.Normalize();

				FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
				SpawnRotation = myMatrix.Rotator();


				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(
					GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile)
				{
					myProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
				}
			}
			else
			{
				FHitResult Hit;
				TArray<AActor*> Actors;

				UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation,
				                                      EndLocation * WeaponSetting.DistanceTrace,
				                                      ETraceTypeQuery::TraceTypeQuery4, false, Actors,
				                                      EDrawDebugTrace::ForDuration,
				                                      Hit, true, FLinearColor::Red, FLinearColor::Green, 0.5f);

				if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
				{
					EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);

					if (WeaponSetting.ProjectileSetting.HitDecals.Contains(mySurfacetype))
					{
						UMaterialInterface* myMaterial = WeaponSetting.ProjectileSetting.HitDecals[mySurfacetype];

						if (myMaterial && Hit.GetComponent())
						{
							UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.0F), Hit.GetComponent(),
							                                     NAME_None, Hit.ImpactPoint,
							                                     Hit.ImpactNormal.Rotation(),
							                                     EAttachLocation::KeepWorldPosition, 5.0f);
						}
					}

					if (WeaponSetting.ProjectileSetting.HitFXs.Contains(mySurfacetype))
					{
						UParticleSystem* myParticle = WeaponSetting.ProjectileSetting.HitFXs[mySurfacetype];
						if (myParticle)
						{
							UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle,
							                                         FTransform(Hit.ImpactNormal.Rotation(),
								                                         Hit.ImpactPoint, FVector(1.0f)));
						}
					}
					if (WeaponSetting.ProjectileSetting.HitSound)
					{
						UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSetting.ProjectileSetting.HitSound,
						                                      Hit.ImpactPoint);
					}

					Utypes::AddEffectBySurfaceType(Hit.GetActor(), ProjectileInfo.Effect, mySurfacetype);


					UGameplayStatics::ApplyPointDamage(Hit.GetActor(), WeaponSetting.ProjectileSetting.ProjectileDamage,
					                                   Hit.TraceStart, Hit,
					                                   GetInstigatorController(), this,NULL);
				}
			}
		}
	}

	if (GetWeaponRound() <= 0 && !WeaponReloading)
	{
		//InitReload
		if (ChekCanWeaponReload())
			InitReload();
	}
}

// Updates the weapon's state based on the character's movement stat
void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{
	BlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::Aim_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::AimWalk_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionReduction;
		break;
	case EMovementState::Walk_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Walk_StateDispersionReduction;
		break;
	case EMovementState::Run_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Run_StateDispersionReduction;
		break;
	case EMovementState::SprintRun_State:
		BlockFire = true;
		SetWeaponStateFire(false); //set fire trigger false
		break;
	default:
		break;
	}
}

// Handles changes in weapon dispersion
void AWeaponDefault::ChangeDispersionByShoot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bIsShootDirection = false;
	FVector EndLocation = FVector(0.f);
	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);
	if (tmpV.Size() > SizeVector)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(
			ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal() * -2000.f;
	}
	else
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector())
			* 2000.0f;


	return EndLocation;
}

int8 AWeaponDefault::GetNumberProjectilesByShot() const
{
	return WeaponSetting.NumberProjectileByShot;
}

int32 AWeaponDefault::GetWeaponRound()
{
	return AdditionalWeaponInfo.Round;
}

void AWeaponDefault::InitReload()
{
	WeaponReloading = true;

	ReloadTimer = WeaponSetting.ReloadTime;

	if (WeaponSetting.FAnimInfo.AnimCharacterReload)
		OnWeaponReloadStart.Broadcast(WeaponSetting.FAnimInfo.AnimCharacterReload);
	if (WeaponSetting.FAnimInfo.AnimCharacterReload)
		OnWeaponReloadStart.Broadcast(WeaponSetting.FAnimInfo.AnimWeaponReload);

	if (WeaponSetting.MagDropMesh.MeshDrop)
	{
		if (WeaponSetting.MagDropMesh.MeshDropTime < 0.0f)
		{
			InitDropMesh(WeaponSetting.MagDropMesh.MeshDrop, WeaponSetting.MagDropMesh.MeshDropOffset,
			             WeaponSetting.MagDropMesh.MeshDropImpulseVector, WeaponSetting.MagDropMesh.MeshDropLifeTime,
			             WeaponSetting.MagDropMesh.ImpulseDispersion, WeaponSetting.MagDropMesh.ImpulsePower,
			             WeaponSetting.MagDropMesh.CustomMass);
		}
		else
		{
			DropMagFlag = true;
			DropMagTimer = WeaponSetting.MagDropMesh.MeshDropTime;
		}
	}


	//ToDO Reload animation
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;

	int8 AvailableAmmoFormInventory = GetAvailableAmmoForReload();
	int8 AmmoNeedsTakenFromInv;
	int8 NeedToReload = WeaponSetting.MaxRound - AdditionalWeaponInfo.Round;

	if (NeedToReload > AvailableAmmoFormInventory)
	{
		AdditionalWeaponInfo.Round = AvailableAmmoFormInventory;
		AmmoNeedsTakenFromInv = AvailableAmmoFormInventory;
	}
	else
	{
		AdditionalWeaponInfo.Round += NeedToReload;
		AmmoNeedsTakenFromInv = NeedToReload;
	}

	OnWeaponReloadEnd.Broadcast(true, -AmmoNeedsTakenFromInv);
}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;
	if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.015f);

	OnWeaponReloadEnd.Broadcast(false, 0);
	DropMagFlag = false;
}

bool AWeaponDefault::ChekCanWeaponReload()
{
	bool result = true;
	if (GetOwner())
	{
		UTDSInventoryComponent* MyInventory = Cast<UTDSInventoryComponent>(
			GetOwner()->GetComponentByClass(UTDSInventoryComponent::StaticClass()));
		if (MyInventory)
		{
			int8 AvailableAmmoForWeapon;
			if (!MyInventory->CheckAmmoForWeapon(WeaponSetting.WeaponType, AvailableAmmoForWeapon))
			{
				result = false;
			}
		}
	}

	return result;
}

int8 AWeaponDefault::GetAvailableAmmoForReload()
{
	int8 AvailableAmmoForReload = WeaponSetting.MaxRound;
	if (GetOwner())
	{
		UTDSInventoryComponent* MyInventory = Cast<UTDSInventoryComponent>(
			GetOwner()->GetComponentByClass(UTDSInventoryComponent::StaticClass()));
		if (MyInventory)
		{
			if (MyInventory->CheckAmmoForWeapon(WeaponSetting.WeaponType, AvailableAmmoForReload))
			{
				AvailableAmmoForReload = AvailableAmmoForReload;;
			}
		}
	}
	return AvailableAmmoForReload;
}

void AWeaponDefault::InitDropMesh(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection,
                                  float lifetime, float ImpulseRandomDispersion, float PowerImpule, float customMass)
{
	if (DropMesh)
	{
		FTransform Transform;

		FVector LocalDir = this->GetActorForwardVector() * Offset.GetLocation().X + this->GetActorRightVector() + Offset
			.GetLocation().Y + this->GetActorUpVector() + Offset.GetLocation().Z;

		Transform.SetLocation(GetActorLocation() + LocalDir);
		Transform.SetScale3D(Offset.GetScale3D());

		Transform.SetRotation(GetActorRotation().Quaternion() * Offset.Rotator().Quaternion());
		AStaticMeshActor* NewActor = nullptr;

		FActorSpawnParameters Param;
		Param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		Param.Owner = this;
		NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Transform, Param);

		if (NewActor && NewActor->GetStaticMeshComponent())
		{
			NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
			NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
			NewActor->SetActorTickEnabled(false);
			NewActor->InitialLifeSpan = lifetime;

			NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
			NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
			NewActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);

			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(
				ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(
				ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(
				ECC_WorldStatic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(
				ECC_WorldDynamic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(
				ECC_PhysicsBody, ECollisionResponse::ECR_Block);


			if (customMass > 0.0f)
			{
				NewActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, customMass, true);
			}
			if (!DropImpulseDirection.IsNearlyZero())
			{
				FVector FinalDir;
				LocalDir = LocalDir + (DropImpulseDirection * 1000.f);

				if (!FMath::IsNearlyZero(ImpulseRandomDispersion))
					FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, ImpulseRandomDispersion);
				FinalDir.GetSafeNormal(0.0001f);
				NewActor->GetStaticMeshComponent()->AddImpulse(FinalDir * PowerImpule);
			}
		}
	}
}
