// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Weapons/ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"


int32 DebugExplodeShow = 0;
FAutoConsoleVariableRef CVAREExplodeShow(
	TEXT("TDSexplodeShow"),
	DebugExplodeShow,
	TEXT("Draw debug for explosion"),
	ECVF_Cheat);

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectileDefault_Grenade::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	TimerExplode(DeltaSeconds);
}

void AProjectileDefault_Grenade::TimerExplode(float DeltaTime)
{
	if (TimerEnabled)
    {
        TimerToExplode += DeltaTime;  // Increment TimerToExplode with DeltaTime
        if (TimerToExplode > TimeToExplode)
        {
            Explode();
        }
    }
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//Init Grenade
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explode()
{
	if(DebugExplodeShow)
	{
		DrawDebugSphere(GetWorld(),GetActorLocation(),ProjectileSetting.ProjectileMinxRadiusDamage,12,FColor::Green,false,12.0f);
		DrawDebugSphere(GetWorld(),GetActorLocation(),ProjectileSetting.ProjectileMaxRadiusDamage,12,FColor::Red,false,12.0f);
	}
	TimerEnabled = false;
	if (ProjectileSetting.ExplodeFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),ProjectileSetting.ExplodeFX,GetActorLocation(),GetActorRotation());
	}
	if (ProjectileSetting.ExplodeSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(),ProjectileSetting.ExplodeSound,GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExplodeMaxDamage,
		ProjectileSetting.ExplodeMaxDamage*0.2,
		GetActorLocation(),
		1000.0f,
		2000.0f,
		5,
		NULL, IgnoredActor, this, nullptr);

	this->Destroy();
}
